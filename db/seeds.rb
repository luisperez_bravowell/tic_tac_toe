# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#

s1 = Score.create(name: "Luis Perez", points: 5)
s2 = Score.create(name: "Matthew Sprankle", points: 4)
s3 = Score.create(name: "Computer", points: 3)