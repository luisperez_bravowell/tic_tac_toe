Rails.application.routes.draw do
	root 'tic_tac_toe#home'
	get '/tic_tac_toe/prize' => 'tic_tac_toe#prize'
	get '/tic_tac_toe/points' => 'tic_tac_toe#points'
	
	resources :scores
	
	post '/tic_tac_toe' => 'tic_tac_toe#create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
