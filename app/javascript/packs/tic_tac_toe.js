// combinations of winning  use .every() method to compare the arrays
// (0,1,2) - (3,4,5) - (6,7,8) - (0,3,6) - (1,4,7) - (2,5,8) - (0,4,8)
const combinations_of_winning = [
	[0,1,2],
	[3,4,5],
	[6,7,8],
	[0,3,6],
	[1,4,7],
	[2,5,8],
	[0,4,8]
];
const circle_type = 'cirlce';
const x_type = 'x';
const selected_first_half = document.querySelectorAll(".toe");
const selected_second_half = document.querySelectorAll(".x-second-half");
const win = 3;

let turn = 0;
let circle_array = [];
let x_array = [];


// logic so that when the correct item displays depending on the turn
[...document.querySelectorAll(".tac")].map( (tac_element) => {
	tac_element.addEventListener('click', (function () {
		if(turn === 0 ) {
			turn++;
			first_half(tac_element.id);
		} else {
			turn = 0
			second_half(tac_element.id);
		}
	}));
});


function first_half(parent_element){
	let selected_box = document.getElementById(parent_element).getElementsByClassName('toe')[0];
	selected_box.classList.add('circle');
	circle_array.push(parent_element);
	let who_wins = check_for_winner(circle_array, circle_type);
	if (who_wins) {
		alert("Cirlce you're winner!");
		document.getElementById('name').value = "Player 1";
		document.getElementById('points').value = "1";
	}
}


function second_half(parent_element){
	let selected_box_first_half = document.getElementById(parent_element).getElementsByClassName('toe')[0];
	let selected_box_second_half = document.getElementById(parent_element).getElementsByClassName('x-second-half')[0];
	selected_box_second_half.classList.remove('hide');
	selected_box_first_half.classList.add('x-first-half');
	x_array.push(parent_element);
	let who_wins = check_for_winner(x_array, x_type);
	if (who_wins) {
		alert("X you're winner!");
		document.getElementById('name').value = "Player 2";
		document.getElementById('points').value = "1";
	}
}


function check_for_winner(compare, type){
	let check_counter = 0;
	let winner = false;

	combinations_of_winning.forEach( combination => {
		compare.forEach( com => {
			combination.forEach( value => {
				if (com == value) check_counter++;
			});
		});
		if (check_counter == 3) winner = true;
		check_counter = 0; //reset
	});
	
	return winner;
}